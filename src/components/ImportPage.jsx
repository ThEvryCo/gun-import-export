import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel, Form, ListGroup, ListGroupItem } from "react-bootstrap";
import _ from 'lodash';
import moment from 'moment';
import ReactFileReader from 'react-file-reader';
import axios from 'axios';

const initialState = { file: ''}

class ImportPage extends Component {
  constructor(props) {
    super(props);

    this.state = initialState
  }

  handleFiles(files) {
    const self = this;
    var user = this.props.user;
    const data = new FormData();
    data.append('file', files[0]);

    axios.post('/importfile', data)
      .then(function (res) {
        console.log(res);
        self.putData(0, res.data);
      })
      .catch(function (err) {
        console.log(err);
        toastr['error']('File not imported');
      });
  }

  putData(state, initialArr, putArr, idObj, comparePart) {
    const self = this;
    var user = this.props.user;
    if(state == 0) {
      console.log('state 0');
      var tempIdObj = {};
      for(let i = 0; i < initialArr.length; i++) {
        if(i && (i % 50 == 0)) {
          //console.log(i);
          localStorage.clear();
        }
        var id = Gun.text.random()
        initialArr[i].importId = id;
        tempIdObj[initialArr[i].importId] = 0;
        
        var getString = 'part/' + initialArr[i].importId;

        user.get(getString).put(initialArr[i]);


        if(i == (initialArr.length - 1)) {
          self.putData(1, initialArr, [], tempIdObj);
        }
      }
    }
    else if(state == 1) {
      console.log('state 1');
      //setTimeout(function() {
        for(let i = 0; i < initialArr.length; i++) {
          if(i && (i % 50 == 0)) {
            //console.log(i);
            localStorage.clear();
          }
          
          var getString = 'part/' + initialArr[i].importId;
          let exist
          user.get(getString).on(function(part) {
            exist = part;
          });

          if(!exist) {
            putArr.push(initialArr[i]);
          }

          if(i == (initialArr.length - 1)) {
            self.putData(2, initialArr, putArr, idObj);
          }
        }
      //}, 5000)
    }
    else if(state == 2) {
      console.log('state 2');
      console.log(putArr.length);
      if(putArr.length == 0) {
        self.putData(3, initialArr, putArr, idObj);
      }
      else {
        for(let i = 0; i < putArr.length; i++) {
          if(i && (i % 50 == 0)) {
            //console.log(i);
            localStorage.clear();
          }
          
          var getString = 'part/' + putArr[i].importId;

          user.get(getString).put(putArr[i]);


          if(i == (putArr.length - 1)) {
            self.putData(1, initialArr, [], idObj);
          }
        }
      }
    }
    else if(state == 3) {
      console.log('state 3');
      console.log(Object.keys(idObj).length);
      if(Object.keys(idObj).length == 0) {
        return;
      }
      else {
        var keys = Object.keys(idObj)
        for(let i = 0; i < keys.length; i++) {
          if(i && (i % 50 == 0)) {
            //console.log(i);
            localStorage.clear();
          }
          
          var getString = 'part/' + keys[i];
          var curPart = user.get(getString);
          user.get('parts').set(curPart);

          //setTimeout(function() {
            if(i == (keys.length - 1)) {
              self.putData(4, initialArr, putArr, idObj);
            }
          //}, 1000);
        }
      }
    }
    else if(state == 4) {
      console.log('state 4');
      //setTimeout(function() {
        let ids = {};
        user.get('parts').on(function(setList) {
          ids = setList;
        });
        let keys = Object.keys(ids) || [];
        for(let i = 0; i < keys.length; i++) {
          gun.get(keys[i]).on(function(part) {
            self.putData(5, initialArr, putArr, idObj, part);
          });
          //setTimeout(function() {
            if(i == (keys.length - 1)) {
              self.putData(3, initialArr, putArr, idObj);
            }
          //}, 1000);
        }
      //}, 5000);
    }
    else if(state == 5) {
      if(idObj[comparePart.importId]) {
        delete idObj[comparePart.importId];
      }
    }
  }

  render() {
    return (
      <div className="text-center">
        <ReactFileReader handleFiles={this.handleFiles.bind(this)}>
          <button className='btn'>Import</button>
        </ReactFileReader>
      </div>
    );
  }
}

export default ImportPage;
