import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel, Form, ListGroup, ListGroupItem } from "react-bootstrap";
import _ from 'lodash';
import moment from 'moment';
import { ExportToCsv } from 'export-to-csv';

const initialState = { entries: [], year: '', week: '', day: ''}

class ExportPage extends Component {
  constructor(props) {
    super(props);

    this.state = initialState
  }

  args(){
    if (this.state.year.length){
      if (this.state.week.length){
        if (this.state.day.length){
          return this.state.year + "/" + this.state.week + "/" + this.state.day
        }
        return this.state.year + "/" + this.state.week
      }
      return this.state.year
    }
    return
  }

  query(event) {
    event.preventDefault();
    const self = this;
    var user = this.props.user;
    var entries = this.state.entries;
    if(entries.length > 0) { entries = [] }
    var duration= this.state.duration;
    if(duration > 0) { duration = 0 }
    var query = this.args()
    this.setState({show: false});

    this.setState({entries}, () => {
      if (query){
        user.get(query).map().on(function(entry, id) {
          let data = { id: id, data: entry}
          const merged = _.merge(data,entry);
          const index = _.findIndex(entries, (o)=>{ return o.id === id});
          if(index >= 0) {
            entries[index] = merged;
          } else {
            entries.push(merged);
            duration += entry.duration
          }

          var sorted = _.sortBy(entries, function(curEntry) {
            if(curEntry.checkInEdit) {
              return curEntry.validity_unix + curEntry.checkInEdit
            }
            else {
              return curEntry.validity_unix + curEntry.checkIn;
            }
          });

          sorted.forEach(function(curEntry) {
            if(!curEntry.checkIn) {
              curEntry.checkIn = '';
            }
            if(!curEntry.checkInEdit) {
              curEntry.checkInEdit = '';
            }

            if(!curEntry.checkOut) {
              curEntry.checkOut = '';
            }
            if(!curEntry.checkOutEdit) {
              curEntry.checkOutEdit = '';
            }
             if(!curEntry.checkOutDisplay) {
              curEntry.checkOutDisplay = '';
            }
            if(!curEntry.checkOutLastModified) {
              curEntry.checkOutLastModified = '';
            }
            if(!curEntry.duration) {
              curEntry.duration = '';
            }
          });

          self.setState({entries: sorted});
          self.setState({duration});
        });
      } else{
        return
      }
    });
  }

  exportClick(event){
    event.preventDefault();

    var now = new Date();
    now = moment(now).format('MM-DD-YYYY-HH:mm');
    var title = 'export-' + now;

    const options = { 
      fieldSeparator: ',',
      fileName: title,
      showLabels: true, 
      useBom: true,
      useKeysAsHeaders: true
      // [
      //   '_'
      //   'id',
      //   'data',
      //   'created', 
      //   'created_day', 
      //   'validity', 
      //   'validity_unix', 
      //   'checkIn', 
      //   'checkInEdit', 
      //   'checkInDisplay', 
      //   'checkInLastModified', 
      //   'checkOut', 
      //   'checkOutEdit', 
      //   'checkOutDisplay', 
      //   'checkOutLastModified', 
      //   'duration', 
      //   'lastModified', 
      //   'project', 
      //   'task', 
      //   'deleted'
      // ],
    };

    const csvExporter = new ExportToCsv(options);
    var exportFile = csvExporter.generateCsv(this.state.entries);
  }

  handleChange(event){
     this.setState({
       [event.target.id]: event.target.value
     });
  }


  getEntryItem(entry) {
    const curEntry = entry;
    return (
      <ListGroupItem key={entry.created} id={entry.created} className="list-group-item list-group-item-action">
        {entry.validity}, Check in: {entry.checkInDisplay} Check out: {entry.checkOutDisplay}
      </ListGroupItem>
    )
  }

  render() {
    return (
      <div className="text-center">
        <h1>Export</h1>
        <Form inline>
          <FormGroup controlId="year">
            <ControlLabel>Year</ControlLabel>{' '}
            <FormControl
              type="text"
              value={this.state.year}
              placeholder="Required"
              onChange={this.handleChange.bind(this)}
             />
          </FormGroup>{' '}
          <FormGroup controlId="week">
            <ControlLabel>Week</ControlLabel>{' '}
            <FormControl
              type="text"
              value={this.state.week}
              onChange={this.handleChange.bind(this)}
             />
          </FormGroup>{' '}
          <FormGroup controlId="day">
            <ControlLabel>Day</ControlLabel>{' '}
            <FormControl
              type="text"
              value={this.state.day}
              onChange={this.handleChange.bind(this)}
             />
          </FormGroup>{' '}
          <Button onClick={this.query.bind(this)} type="submit">Search</Button>
        </Form>
        <Button onClick={this.exportClick.bind(this)}>Export</Button>
        <ListGroup className="text-center">
          {this.state.entries.map(this.getEntryItem.bind(this))}
        </ListGroup>

      </div>
    );
  }
}

export default ExportPage;
