import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Gun from 'gun/gun';
import SEA from 'gun/sea'


import NavBar from './NavBar.jsx'
import Home from './Home.jsx';
import Profile from './Profile.jsx';
import ExportPage from './ExportPage.jsx'
import ImportPage from './ImportPage.jsx'
import PartsView from './PartsView.jsx'

class App extends Component {
  constructor() {
  super();
    let opt = {}
    opt.peers = ["http://localhost:8080/gun"]
    opt.localStorage = false
    this.gun = Gun(opt);
    window.gun = this.gun;
    this.user = this.gun.user();
    window.user = this.user;
    this.state = {
      loggedIn: false
    }
  }

  render() {
    return (
      <Router>
        <div>
          {this.state.loggedIn && <NavBar user={this.user} />}
          {!this.state.loggedIn && <Route exact path="/"
            render={(props) => <Home {...props} user={this.user} onUpdate={this.onUpdate.bind(this)} />}
          />}
          <Route path="/profile"
            render={(props) => <Profile {...props} user={this.user} username={this.state.username} onUpdate={this.onUpdate.bind(this)} />}
          />
          <Route path="/export"
            render={(props) => <ExportPage {...props} user={this.user} />}
          />
          <Route path="/import"
            render={(props) => <ImportPage {...props} user={this.user} />}
          />
          <Route path="/parts"
            render={(props) => <PartsView {...props} user={this.user} />}
          />
        </div>
      </Router>
    );
  }
  onUpdate (loggedIn, username) { this.setState({loggedIn: loggedIn}) }
}

export default App;
