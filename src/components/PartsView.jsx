import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel, Form, ListGroup, ListGroupItem } from "react-bootstrap";
import _ from 'lodash'
import moment from 'moment';

class PartsView extends Component {
  constructor(props) {
    super(props);

    this.state = { parts: [] }
  }

  componentDidMount() {
    const self = this;
    var user = this.props.user;
    var parts = this.state.parts;
    if(parts.length > 0) { parts = [] }
    
    user.get('parts').map().on(function(part, id) {
      let data = { id: id, data: part}
        const merged = _.merge(data,part);
        const index = _.findIndex(parts, (o)=>{ return o.id === id});
        if(index >= 0) {
          parts[index] = merged;
        } else {
          parts.push(merged);
        }

        self.setState({parts: parts});
    });
  }

  buttonClick(partId){
    const self = this;
    var user = this.props.user;
    var query = 'part/' + partId;   
    user.get(query).once(function(part) {
      if(part) {
        console.log(part);  
      }
    });
  }

  getPartItem(part) {
    const curPart = part;
    return (
      <Button key={part.importId} id={part.importId} onClick={() => {this.buttonClick(curPart.importId)}} className="list-group-item list-group-item-action">
        {part.importId}
      </Button>
    )
  }

  render() {
    return (
      <div className="text-center">
        <h1>Parts</h1>
        <ListGroup>
          {this.state.parts ? null : null}
        </ListGroup>
      </div>
    );
  }
  showStatus () { return this.state.show }
  closeModal () { this.setState({show: false}) }
}

export default PartsView;
