import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap'
import { Link } from 'react-router-dom'

class NavBar extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      show: false
    };
  }

  toggleModal() {
    var show = !this.state.show;
    this.setState({show});
  }

  render() {
    return (
      <div>
        <Navbar id="topNav" fluid>
          <Navbar.Header>
            <Navbar.Brand>
              <Navbar.Link componentClass={Link} href="/parts" to="/parts">
               Parts
              </Navbar.Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              <NavItem eventKey={2} componentClass={Link} href="/" to="/profile">
                Profile
              </NavItem>
              <NavItem eventKey={1} componentClass={Link} href="/" to="/import">
                Import
              </NavItem>
              <NavItem eventKey={1} componentClass={Link} href="/" to="/export">
                Export
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
  showStatus () { return this.state.show }
  closeModal () { this.setState({show: false}) }
}

export default NavBar;
